<?php
require_once("include/fct.inc.php");
require_once ("include/class.pdogsb.inc.php");
session_start();
$pdo = PdoGsb::getPdoGsb();
$estConnecte = estConnecte();
$uc = $_REQUEST['uc'];
if(!isset($_REQUEST['uc']) || $_REQUEST['uc'] !='genererPDF' || !$estConnecte){
	 include("vues/v_entete.php");
     $_REQUEST['uc'] = 'connexion';
}
switch($uc){
	case 'connexion':{
		include("controleurs/c_connexion.php");
		break;
	}
	case 'gererFrais' :{
		include("controleurs/c_gererFrais.php");
		break;
	}
	case 'etatFrais' :{
		include("controleurs/c_etatFrais.php");
		break; 
	}
	case 'modifEtat':{
		include("controleurs/c_suivrePaiement.php");
		break;
	}
	case 'voirFicheFraisClient':{
		$idClient = $_REQUEST["idClient"];
		include("controleurs/c_suivrePaiement.php");
		break;
	}
	case 'RembourseFiche':{
		echo $idFiche = $_REQUEST["idFiche"];
		echo $idClient = $_REQUEST["idClient"];
		include("controleurs/c_suivrePaiement.php");
		break;
	}
	case 'genererPDF':{
		include("controleurs/c_etatFrais.php");
		break;
	}
}
if($uc !='genererPDF'){
	include("vues/v_pied.php") ;
}
?>

