<div id="contenu">
<h3>Fiches du client n°<?php echo $idClient ?></h3>
<?php if($Client != null){ ?>
<table class="listeLegere">
<caption>Fiche forfait</caption>
<thead>
    <tr>
        <th>mois</th>
        <th>quantité</th>
        <th>libelle</th>
        <th>etat</th>
        <th>rembourser</th>

    </tr>
</thead>
<?php
foreach($Client as $ficheFraisForfait){
    $mois = $ficheFraisForfait["mois"];
    $quantite = $ficheFraisForfait["quantite"];
    $id = $ficheFraisForfait["idFraisForfait"];
    $etat = $ficheFraisForfait["idEtat"];


    /*$_SESSION["date"] = $dateModif;
    $_SESSION["nbJustificatifs"] = $nbJustificatifs;
    $_SESSION["montantValide"] = $montantValide;
    $_SESSION["Etat"] = $Etat;
    $_SESSION["client"] = $idClient;*/
?>
<tbody>
    <tr>
        <td><?php echo $mois ?></td>
        <td><?php echo $quantite ?></td>
        <td><?php echo $id ?></td>
        <td><?php echo $etat ?></td>
        <td><a href="index.php?uc=RembourseFiche&action=Rembourser">valider</a></td>
    </tr>
</tbody>
<?php } ?>
</table>
<?php } else{
    echo "Aucune fiche forfait disponible pour ce mois";
} ?> 
<br>
<br>
<?php if($ClientHf != null) {?>
<table class="listeLegere">
<caption>Fiche hors forfait</caption>
<thead>
    <tr>
        <th>idFiche</th>
        <th>mois</th>
        <th>libelle</th>
        <th>date</th>
        <th>montant</th>
        <th>etat</th>
        <th>rembourser</th>
    </tr>
</thead>
<?php
foreach($ClientHf as $ficheFraisHorsForfait){
    $idFiche = $ficheFraisHorsForfait["id"];
    $mois = $ficheFraisHorsForfait["mois"];
    $libelle = $ficheFraisHorsForfait["libelle"];
    $date = $ficheFraisHorsForfait["date"];
    $montant = $ficheFraisHorsForfait["montant"];
    $etat = $ficheFraisForfait["idEtat"];
?>
<tbody>
    <tr>
        <td><?php echo $idFiche ?></td>
        <td><?php echo $mois ?></td>
        <td><?php echo $libelle ?></td>
        <td><?php echo $date ?></td>
        <td><?php echo $montant ?></td>
        <td><?php echo $etat ?></td>
        <td><a href="index.php?uc=RembourseFiche&action=Rembourser&idFiche=<?php echo $idFiche ?>&idClient=<?php echo $idClient ?>">valider</a></td>
    </tr>
</tbody>
<?php } ?>
</table>
<?php } else{
    echo "Aucune fiche hors forfait disponible pour ce mois";
}  ?>