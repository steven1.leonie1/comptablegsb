<?php
// permet d'inclure la bibliothèque fpdf
require('fpdf/fpdf.php');
// instancie un objet de type FPDF qui permet de créer le PDF
$pdf=new FPDF();
// ajoute une page
$pdf->AddPage();
// définit la police courante
$pdf->SetFont('Arial','B',16);
// Ajout image
$pdf->Image('images/logo.jpg',10,1, 60, 38);
// Décalage à droite
$pdf->Cell(70);
// Titre
$pdf->Cell(70,10,'Recapitulatif inscription',1,10,'C');
// Position de l'entête à 10mm des infos (48 + 10)
$position_entete = 58;
function entete_table($position_entete){
    global $pdf;
    $pdf->SetDrawColor(183); // Couleur du fond
    $pdf->SetFillColor(221); // Couleur des filets
    $pdf->SetTextColor(0); // Couleur du texte
    $pdf->SetY($position_entete);
    $pdf->SetX(8);
    $pdf->Cell(50,8,'Date',1,0,'L',1);
    $pdf->SetX(30);
    $pdf->Cell(30,8,'Libelle',1,0,'C',1);
    $pdf->SetX(60);
    $pdf->Cell(40,8,'Montant',1,0,'C',1);
    $pdf->Ln(); // Retour à la ligne
}
entete_table($position_entete);
// Liste des détails
$position_detail = 66; // Position à 8mm de l'entête
foreach($fraisHorsForfait as $ligne){
    $pdf->SetY($position_detail);
    $pdf->SetX(8);
    $pdf->MultiCell(40,16,utf8_decode($ligne['date']),1,'L');
    $pdf->SetY($position_detail);
    $pdf->SetX(48);
    $pdf->MultiCell(120,8,$ligne['libelle'],1,'C');
    $pdf->SetY($position_detail);
    $pdf->SetX(168);
    $pdf->MultiCell(24,16,$ligne['montant'],1,'R');
    $position_detail += 8;
}
// Enfin, le document est terminé et envoyé au navigateur grâce à Output().
$pdf->Output();
?>