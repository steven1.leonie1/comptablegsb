<div id="contenu">
<table class="listeLegere">
    <caption>Liste des clients</caption>
        <thead>
            <tr>
                <th>Nom</th>
                <th>Prenom</th>
                <th>Fiche(s)</th>
            </tr>
        </thead>
        <?php
    foreach($Comptable as $client){
        $idClient = $client["id"];
        $nom = $client["nom"];
        $prenom = $client["prenom"];
    ?>
        <tbody>
            <tr>
                <td><?php echo $nom ?></td>
                <td><?php echo $prenom ?></td>
                <td><a href="index.php?uc=voirFicheFraisClient&action=FicheDemandeValidation&idClient=<?php echo $idClient ?>">voir</a></td>
            </tr>
        </tbody>
    <?php } ?>
</table>
<div class="piedForm">
</div>