﻿<form method="POST" action="index.php?uc=etatFrais&action=ModifierFrais">
          <input type="hidden" name="lstMois" value="<?php echo  $leMois ?>">
          <input type="hidden" name="idClient" value="<?php echo $idClient ?>">
<h3>Fiche de frais du mois <?php echo $numMois."-".$numAnnee?> : 
    </h3>
    <div class="encadre">
    <p>
        Etat : <?php echo $libEtat?> depuis le <?php echo $dateModif?> <br> Montant validé : <?php echo $montantValide?>
              
                     
    </p>
  	<table class="listeLegere">
  	   <caption>Eléments forfaitisés </caption>
        <tr>
         <?php
         foreach ( $lesFraisForfait as $unFraisForfait ) 
		 {
			$libelle = $unFraisForfait['libelle'];
		?>	
			<th> <?php echo $libelle?></th>
		 <?php
        }
		?>
		</tr>
        <tr>
<?php
          foreach (  $lesFraisForfait as $unFraisForfait  ) 
		  { 
        ?>

          <?php 

				$quantite = $unFraisForfait['quantite'];
		?>
                <td class="qteForfait"><input type="text" name="Forfaits[<?php echo  $unFraisForfait['idfrais'] ?>]" value="<?php echo $quantite?>"> </td>
		 <?php 
          }
		?>

		</tr>
    </table>
       <input style="margin-left:90%; " type="submit" value="Modifier">
  </form>
  <form method="POST" action="index.php?uc=etatFrais&action=ValiderFrais">
  <input type="hidden" name="leMois" value="<?php echo  $leMois ?>">
  <input type="hidden" name="idClient" value="<?php echo $idClient ?>">
  <input type="hidden" name="Montant" value="<?php echo  $montantValide ?>">
  <input type="hidden" name="nbJustificatifs" value="<?php echo  $nbJustificatifs ?>">
  <input type="hidden" name="date" value="<?php echo  $today = date("Y-m-d");  ?>">
  	<table class="listeLegere">
  	   <caption>Descriptif des éléments hors forfait -<?php echo $nbJustificatifs ?> justificatifs reçus -
       </caption>
             <tr>
                <th class="date">Date</th>
                <th class="libelle">Libellé</th>
                <th class='montant'>Montant</th>  
                <th class='Refuser'>Refuser</th>
                <th class='Reporter'>Reporter</th>
                <th class='pdf'>Télécharger</th>  
             </tr>
        <?php      
          foreach ( $lesFraisHorsForfait as $unFraisHorsForfait ) 
		  {
			$date = $unFraisHorsForfait['date'];
			$libelle = $unFraisHorsForfait['libelle'];
			$montant = $unFraisHorsForfait['montant'];
      $idFiche = $unFraisHorsForfait['id'];
      $etatFiche = $unFraisHorsForfait['idEtat'];
      $idVisiteur = $unFraisHorsForfait['idVisiteur'];
      $mois = $unFraisHorsForfait['mois'];
      $libelle = $unFraisHorsForfait['libelle'];
		?>
             <tr>
              <?php if($etatFiche == "CL"){
                echo "Refuser";
              } ?>
                <td><?php echo $date ?></td>
                <td><?php echo $libelle ?></td>
                <td><?php echo $montant ?></td>
                <td><a href="index.php?uc=etatFrais&action=Refuser&idFiche=<?php echo $idFiche ?>&idVisiteur=<?php echo $idVisiteur ?>&mois=<?php echo $mois ?>&libelle=<?php echo $libelle ?>&montantASoustraire=<?php echo $montant ?>">Refuser</a></td>
                <td><a href="index.php?uc=etatFrais&action=reporterDate&idFiche=<?php echo $idFiche ?>&idVisiteur=<?php echo $idVisiteur ?>&dateFiche=<?php echo $date ?>&libelle=<?php echo $libelle ?>&montant=<?php echo $montant ?>">Reporter</a></td></td>
                <td><a href="index.php?uc=genererPDF&action=voirPDF&idVisiteur=<?php echo $idVisiteur ?>&idFiche=<?php echo $idFiche ?>&libelle=<?php echo $libelle ?>&montantASoustraire=<?php echo $montant ?>" target="_blank"><img src="images/pdf.png"></img></a></td>
             </tr>
        <?php 
          }
		?>
    </table>
    <input style="margin-left:90%; " type="submit" value="Valider">
  </div>
  </div>
  </form>
  <?php if(isset($_POST['etape'])){
    echo $_POST['etape'];
  }
