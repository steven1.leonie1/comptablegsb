﻿<?php
$action = $_REQUEST['action'];
if($action != 'voirPDF'){
	include("vues/v_sommaire.php");
}
$idVisiteur = $_SESSION['idVisiteur'];
switch($action){

	case 'selectionnerClient':{
			$ClientDuComptable =  $pdo->getClientComptable($idVisiteur);
	    	include("vues/v_listeClient.php");
			break;
	}
	case 'selectionnerMois':{
		$idClient = $_REQUEST['lstClient'];
		$lesMois=$pdo->getLesMoisDisponibles($idClient);
		// Afin de sélectionner par défaut le dernier mois dans la zone de liste
		// on demande toutes les clés, et on prend la première,
		// les mois étant triés décroissants
		$lesCles = array_keys( $lesMois );
		$moisASelectionner = $lesCles[0];
		include("vues/v_listeMois.php");
		break;
	}
	case 'voirEtatFrais':{
		$leMois = $_REQUEST['lstMois'];
		$idClient = $_REQUEST["idClient"]; 
		$lesFraisHorsForfait = $pdo->getLesFraisHorsForfait($idClient,$leMois);
		$lesFraisForfait= $pdo->getLesFraisForfait($idClient,$leMois);
		$lesInfosFicheFrais = $pdo->getLesInfosFicheFrais($idClient,$leMois);
		$numAnnee =substr( $leMois,0,4);
		$numMois =substr( $leMois,4,2);
		$libEtat = $lesInfosFicheFrais['libEtat'];
		$montantValide = $lesInfosFicheFrais['montantValide'];
		$nbJustificatifs = $lesInfosFicheFrais['nbJustificatifs'];
		$dateModif =  $lesInfosFicheFrais['dateModif'];
		$dateModif =  dateAnglaisVersFrancais($dateModif);
		include("vues/v_etatFrais.php");
		break;
	}
	case 'Refuser':{
		$idFiche = $_REQUEST['idFiche']; 
		$idVisiteur = $_REQUEST['idVisiteur']; 
		$mois = $_REQUEST['mois']; 
		$libelle =  $_REQUEST['libelle'];
		$montantASoustraire = $_REQUEST['montantASoustraire'];
		$modifierRefuser = $pdo->updateFicheFraisHorsForfait($idVisiteur,$idFiche,$libelle);
		$Soustraire = $pdo->soustraireMontant($idFiche,$idVisiteur,$mois,$montantASoustraire);
		header('Location: index.php?uc=etatFrais&action=selectionnerClient');
		break;
	}
	case 'reporterDate':{
		$currentDate = date("Y-m-d");  
		$idFiche = $_REQUEST['idFiche'];
		$idVisiteur = $_REQUEST['idVisiteur'];
		$dateFicheFR = $_REQUEST['dateFiche'];
		$mois = getMois($currentDate);
		$dateFicheEN = dateFrancaisVersAnglais($dateFicheFR);
		$reportDate = $pdo->reportMois($dateFicheEN, $idVisiteur, $idFiche);
		break;
	}
	case 'ModifierFrais':{
		$leMois = $_REQUEST['lstMois'];
		$idClient = $_REQUEST["idClient"]; 
		$lesForfaits = $_REQUEST['Forfaits'];
		$maj = $pdo->majFraisForfait($idClient,$leMois,$lesForfaits);
		$leMois = $_REQUEST['lstMois'];
		$idClient = $_REQUEST["idClient"]; 
		$lesFraisHorsForfait = $pdo->getLesFraisHorsForfait($idClient,$leMois);
		$lesFraisForfait= $pdo->getLesFraisForfait($idClient,$leMois);
		$lesInfosFicheFrais = $pdo->getLesInfosFicheFrais($idClient,$leMois);
		$numAnnee =substr( $leMois,0,4);
		$numMois =substr( $leMois,4,2);
		$libEtat = $lesInfosFicheFrais['libEtat'];
		$montantValide = $lesInfosFicheFrais['montantValide'];
		$nbJustificatifs = $lesInfosFicheFrais['nbJustificatifs'];
		$dateModif =  $lesInfosFicheFrais['dateModif'];
		$dateModif =  dateAnglaisVersFrancais($dateModif);
		include("vues/v_etatFrais.php");
		break;
	}
	case 'ValiderFrais':{
		$leMois = $_REQUEST['leMois'];
		$idClient = $_REQUEST["idClient"]; 
		$montant = $_REQUEST["Montant"]; 
		$nbJustificatifs = $_REQUEST["nbJustificatifs"]; 
		$date = $_REQUEST["date"]; 
		$pdo->validerFicheFrais($idClient,$leMois,$nbJustificatifs,$montant,$date);

		$lesFraisHorsForfait = $pdo->getLesFraisHorsForfait($idClient,$leMois);
		$lesFraisForfait= $pdo->getLesFraisForfait($idClient,$leMois);
		$lesInfosFicheFrais = $pdo->getLesInfosFicheFrais($idClient,$leMois);
		$numAnnee =substr( $leMois,0,4);
		$numMois =substr( $leMois,4,2);
		$libEtat = $lesInfosFicheFrais['libEtat'];
		$montantValide = $lesInfosFicheFrais['montantValide'];
		$nbJustificatifs = $lesInfosFicheFrais['nbJustificatifs'];
		$dateModif =  $lesInfosFicheFrais['dateModif'];
		$dateModif =  dateAnglaisVersFrancais($dateModif);
		include("vues/v_etatFrais.php");
		break;
	}
	case 'voirPDF':{
		$idVisiteur = $_REQUEST['idVisiteur'];
		$idFiche = $_REQUEST['idFiche'];
		$fraisHorsForfait = $pdo->getFraisHorsForfaitV2($idVisiteur, $idFiche);
		include("vues/v_pdf.php");
		break;
	}
}
?>